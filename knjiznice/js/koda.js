var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 /*
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
} */


function zacniGeneriranje() {
	//generirajPodatke(1);
	//generirajPodatke(2);
	//generirajPodatke(3);
	for(var i=1; i<4; i++){
		generirajPodatke(i);
	}
	alert("Vzorčni uporabniki zgenerirani!");
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	ehrId = "";
	///////////////////

	var names = ["David", "Frank", "Paul"];
	var surnames = ["Laid","Obese","Sick"];
	var tezaA = [85,82,59];
	var visinaA = [185,170,182];
	var sessionId = getSessionId();
	console.log("Sejaaa: "+sessionId)
	var ime = names[stPacienta-1];
	var priimek = surnames[stPacienta-1];
	var datumRojstva = "1993-02-02" + "T00:00:00.000Z";
	var telesnaVisina = visinaA[stPacienta-1];
	var telesnaTeza = tezaA[stPacienta-1];
	var sistolicniKrvniTlak = 170;
	var diastolicniKrvniTlak = 120;
	var nasicenostKrviSKisikom = 100;
	var telesnaTemperatura = 0; var datumInUra = "2018-11-21T11:40Z";

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
			async: false,
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
					async: false,
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
						
						
		                if (party.action == 'CREATE') {
							var dodatek; 
							if (stPacienta == 1) {
								dodatek = '<option value=""></option><option value="'+ehrId+'">'+ime+' '+priimek+'</option>';
								$("#preberiObstojeciEHR").html(dodatek);
							} else if (stPacienta == 2) {
								dodatek = '<option value="'+ehrId+'">'+ime+' '+priimek+'</option>';
								$("#preberiObstojeciEHR").append(dodatek);
							} else if (stPacienta == 3) {
								dodatek = '<option value="'+ehrId+'">'+ime+' '+priimek+'</option>';
								$("#preberiObstojeciEHR").append(dodatek);
							}
		                }
						
						$.ajaxSetup({
							headers: {"Ehr-Session": sessionId}
						});
						
						var podatki = {
							// Struktura predloge je na voljo na naslednjem spletnem naslovu:
							// https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
							"ctx/language": "en",
							"ctx/territory": "SI",
							"ctx/time": datumInUra,
							"vital_signs/height_length/any_event/body_height_length": telesnaVisina,
							"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
							"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
							"vital_signs/body_temperature/any_event/temperature|unit": "°C",
							"vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
							"vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
							"vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
							ehrId: ehrId,
							templateId: 'Vital Signs',
							format: 'FLAT',
							committer: 'gm08'
						};
						
						$.ajax({
							url: baseUrl + "/composition?" + $.param(parametriZahteve),
							type: 'POST',
							async: false,
							contentType: 'application/json',
							data: JSON.stringify(podatki),
							success: function (res) {
								//$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-success fade-in'>" + res.meta.href + ".</span>");
							},
							error: function(err) {
								//$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" +JSON.parse(err.responseText).userMessage + "'!");
							}
						});
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	
		
	
	///////////////////////////////
	// TODO: Potrebno implementirati
	return ehrId;
}







// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
//function kreirajEHRzaBolnika() {
function kreirajEHRzaOsebo() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var teza = $("#dodajTezo").val();
	var visina = $("#dodajVisino").val();

		            if (!ime || !priimek || !visina || !teza || ime.trim().length == 0 || priimek.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr", 
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            partyAdditionalInfo: [{key: "height", value: visina}, {key: "weight", value: teza}, {key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  // uspešno
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    
		                    var datumInUra = "2018-11-21T11:40Z";
							var telesnaVisina = visina;
							var telesnaTeza = teza;
							var telesnaTemperatura = "36.50";
							var sistolicniKrvniTlak = "118";
							var diastolicniKrvniTlak = "92";
							var nasicenostKrviSKisikom = "98";
							var merilec = "gm08";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								$.ajaxSetup({
								    headers: {"Ehr-Session": sessionId}
								});
								var podatki = {
									// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
								};
								var parametriZahteve = {
								    "ehrId": ehrId,
								    templateId: 'Vital Signs',
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    success: function (res) {
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'</span>");
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                    
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
 var BMIzaGraf = 0;
//function preberiEHRodBolnika() {
	function racunBMIzaOsebo() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		
		var tT = 0;
		$.ajax({
                url: baseUrl + "/view/" + ehrId + "/" + "weight",
                type: 'GET',
                headers: {"Ehr-Session": sessionId},
                success: function (res) {
                    tT = res[0].weight;
                },
                error: function() {
                    $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" +JSON.parse(err.responseText).userMessage + "'!");
                }
        	});
        	var vV=0;
        	$.ajax({
                url: baseUrl + "/view/" + ehrId + "/" + "height",
                type: 'GET',
                headers: {"Ehr-Session": sessionId},
                success: function (res) {
                    vV = res[0].height;
                },
                error: function() {
                    $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" +JSON.parse(err.responseText).userMessage + "'!");
                }
        	});	
		
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				var str = JSON.stringify(party);
				//var ena = party.partyAdditionalInfo[0].value;
				//var dve = party.partyAdditionalInfo[1].value;
				//var tri = party.partyAdditionalInfo[2].value;
				//var BMI = 0;
				//var BMR = 0;
				/*
				var v = 0;
				var t = 0;
				for(var i=0; i<3; i++){
					if(party.partyAdditionalInfo[i].key === "height")
						v = party.partyAdditionalInfo[i].value;
					if(party.partyAdditionalInfo[i].key === "weight")
						t = party.partyAdditionalInfo[i].value;
				} */
				var BMR = 10*tT+6.25*vV - 5*20;
				BMR = BMR * 1.5;
				var v1 = vV/100;
				var BMI = tT/(v1*v1);
				var BMI1 = BMI.toFixed(2);
				var sporocilo = "Vaš BMI znaša "+BMI1+".";
				if(BMI > 25){
					BMR -= 500;
					sporocilo += " Vrednost vašega indeksa telesne mase presega zdrave vrednosti, ki se gibljejo med 18,5 in 25. Da bi dosegli vrednost v tem intervalu dnevno zaužijte "+BMR+" kalorij. Vaša BMI vrednost se nahaja na stolpcu obarvanem rdeče."
				}
				else if(BMI < 18.5){
					BMR += 500;
					sporocilo += " Vrednost vašega indeksa telesne mase je nižja od vrednosti, ki so značilne za zdravega človeka in znašajo od 18,5 do 25. Da bi dosegli vrednost v tem intervalu dnevno zaužijte "+BMR+" kalorij. Vaša BMI vrednost se nahaja na stolpcu obarvanem rdeče."
				}
				else{
					sporocilo += " Vaš indeks telesne mase ima vrednost, ki je značilna za zdravega človeka, zato nadaljujte z dosedanjo prehrano. Priporočena vrednost dnevno zaužitih kalorij za vas znaša "+BMR+" kalorij. Vaša BMI vrednost se nahaja na stolpcu obarvanem rdeče."
				}
				document.getElementById("racun").textContent=sporocilo;
				BMIzaGraf = BMI1;
				drawChart();

				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Oseba '" + party.firstNames + " " +
          party.lastNames +"' višina: "+vV+" cm, teža: "+tT+" kg</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

///////
var data = {
  "dimension": [
                 {
                   "label": "GHO",
                   "display": "Indicator"
                 },
                 {
                   "label": "PUBLISHSTATE",
                   "display": "PUBLISH STATES"
                 },
                 {
                   "label": "YEAR",
                   "display": "Year"
                 },
                 {
                   "label": "REGION",
                   "display": "WHO region"
                 },
                 {
                   "label": "WORLDBANKINCOMEGROUP",
                   "display": "World Bank income group"
                 },
                 {
                   "label": "AGEGROUP",
                   "display": "Age Group"
                 },
                 {
                   "label": "SEX",
                   "display": "Sex"
                 }
               ],
  "fact": [      
  			          {
             "dim": {
                      "YEAR": "1979",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "SEX": "Male",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "21.7 [21.4-21.9]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1994",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "REGION": "(WHO) Global"
                    },
             "Value": "22.7 [22.6-22.8]"
           },
           {
             "dim": {
                      "SEX": "Male",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1996",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.8 [22.7-22.9]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2011",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Male"
                    },
             "Value": "23.9 [23.8-24.0]"
           },
  			{
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "SEX": "Male",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "YEAR": "2013",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "24.1 [23.9-24.2]"
           }
          ]
};



function getData() {
	/*
		var adef = "";
	for(var i in data333.fact){
		if(i > data333.fact.length - 6);
			adef += data333.fact[i].dim.YEAR+" ";
	}
	*/
	
	//var ena = data.fact[0].Value;
	//var dve = data.fact[0].dim.YEAR;
	//alert(ena+" "+dve);
			/*
			for(var i=1; i<6; i++){
				var t1 = document.getElementById("tab");
				// Create an empty <tr> element and add it to the 1st position of the table:
				var row = t1.insertRow(i);

				// Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
				var cell1 = row.insertCell(0);
				var cell2 = row.insertCell(1);

				// Add some text to the new cells:
				cell1.innerHTML = data.fact[i-1].Value;
				cell2.innerHTML = data.fact[i-1].dim.YEAR;
			}*/
			
			var t1 = document.getElementById("tab");
			for(var i in data333.fact){
				if(i > data333.fact.length - 6){
					var row = t1.insertRow();
					var cell1 = row.insertCell(0);
					var cell2 = row.insertCell(1);
					cell1.innerHTML = data333.fact[i].Value;
					cell2.innerHTML = data333.fact[i].dim.YEAR;
				}
			}
}
///////


function drawChart() {
		var alfa = ["< 18.5","18.5-22","22-25","25-28","28-31","> 31"];
		var romeo = [5.4,22.1,38.6,20.9,10.9,2.1];
		var colore1 = [0,0,0,0,0,0];
		var colore2 = ['blue','red'];
		if(BMIzaGraf < 18.5)
			colore1[0] = 1;
		if(BMIzaGraf > 18.5 && BMIzaGraf < 22)
			colore1[1] = 1;
		if(BMIzaGraf > 22 && BMIzaGraf < 25)
			colore1[2] = 1;
		if(BMIzaGraf > 25 && BMIzaGraf < 28)
			colore1[3] = 1;
		if(BMIzaGraf > 28 && BMIzaGraf < 31)
			colore1[4] = 1;
		if(BMIzaGraf > 31)
			colore1[5] = 1;
			
		var data = google.visualization.arrayToDataTable([
         ['Range', 'Percentage', { role: 'style' }],
         ['< 18.5', 5.4, colore2[colore1[0]]],            // RGB value
         ['18.5-22', 22.1, colore2[colore1[1]]],            // English color name
         ['22-25', 38.6, colore2[colore1[2]]],
       ['25-28', 20.9, colore2[colore1[3]] ], // CSS-style declaration
       ['28-31',10.9,colore2[colore1[4]]],
       ['> 31',2.1,colore2[colore1[5]]]
      ]);
		
      // Define the chart to be drawn.
      //var data = new google.visualization.DataTable();
      //data.addColumn('string', 'Range');
      //data.addColumn('number', 'Percentage');
      /*data.addRows([
        ['< 18.5', 5.4, colore2[colore1[0]]],
        ['18.5-22', 22.1, colore2[colore1[1]]],
        ['22-25', 38.6, colore2[colore1[2]]],
        ['25-28', 20.9, colore2[colore1[3]]],
        ['28-31', 10.9, colore2[colore1[4]]],
        ['> 31', 2.1, colore2[colore1[5]]],
      ]);*/
      /*data.addRows([
        ['< 18.5', 5.4],
        ['18.5-22', 22.1],
        ['22-25', 38.6],
        ['25-28', 20.9],
        ['28-31', 10.9],
        ['> 31', 2.1],
      ]);*/
		/*for(var i=0; i<5; i++){
			var date = data.fact[i].dim.YEAR;
			var dateS = date.toString();
			var bmi1 = data.fact[i].Value;
			var bmiS = bmi1.toString;
			var bmir = bmiS.split(" ");
			var bmiR = Number(bmir[0]);
			data.addRows([
			[dateS,bmiR]
			]);
		}*/
      // Instantiate and draw the chart.
      var chart = new google.visualization.ColumnChart(document.getElementById('graf'));
      chart.draw(data, null);
    }








$(document).ready(function() {
	document.getElementById("racun").textContent="Čakanje na zahtevo za izračun BMI-ja.";

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
	//drawChart();
	getData();
});
