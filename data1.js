var data333 = 
{
  "dimension": [
                 {
                   "label": "GHO",
                   "display": "Indicator"
                 },
                 {
                   "label": "PUBLISHSTATE",
                   "display": "PUBLISH STATES"
                 },
                 {
                   "label": "YEAR",
                   "display": "Year"
                 },
                 {
                   "label": "REGION",
                   "display": "WHO region"
                 },
                 {
                   "label": "WORLDBANKINCOMEGROUP",
                   "display": "World Bank income group"
                 },
                 {
                   "label": "AGEGROUP",
                   "display": "Age Group"
                 },
                 {
                   "label": "SEX",
                   "display": "Sex"
                 }
               ],
  "fact": [
           {
             "dim": {
                      "SEX": "Both sexes",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1981",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.0 [21.9-22.2]"
           },
           {
             "dim": {
                      "YEAR": "1983",
                      "SEX": "Both sexes",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.2 [22.0-22.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "SEX": "Both sexes",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1990",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "22.6 [22.5-22.7]"
           },
           {
             "dim": {
                      "SEX": "Both sexes",
                      "REGION": "(WHO) Global",
                      "YEAR": "1992",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.7 [22.6-22.8]"
           },
           {
             "dim": {
                      "AGEGROUP": "18+  years",
                      "SEX": "Both sexes",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2015",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "REGION": "(WHO) Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "24.4 [24.3-24.5]"
           },
           {
             "dim": {
                      "AGEGROUP": "18+  years",
                      "YEAR": "1975",
                      "REGION": "(WHO) Global",
                      "SEX": "Female",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "21.9 [21.5-22.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "SEX": "Female",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1982",
                      "REGION": "(WHO) Global"
                    },
             "Value": "22.3 [22.1-22.5]"
           },
           {
             "dim": {
                      "REGION": "(WHO) Global",
                      "SEX": "Female",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1984",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.4 [22.2-22.6]"
           },
           {
             "dim": {
                      "YEAR": "2007",
                      "REGION": "(WHO) Global",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "24.0 [23.9-24.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "YEAR": "2009"
                    },
             "Value": "24.1 [24.0-24.2]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "YEAR": "2014",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "24.5 [24.3-24.6]"
           },
           {
             "dim": {
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2016",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "24.6 [24.4-24.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1997",
                      "REGION": "(WHO) Global",
                      "SEX": "Male",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "22.9 [22.8-23.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "SEX": "Male",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1999",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "23.0 [22.9-23.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Male",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "2006",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "23.5 [23.4-23.6]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "SEX": "Male",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2008"
                    },
             "Value": "23.7 [23.6-23.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Both sexes",
                      "REGION": "(WHO) Global",
                      "YEAR": "1985",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "22.3 [22.2-22.4]"
           },
           {
             "dim": {
                      "AGEGROUP": "18+  years",
                      "SEX": "Both sexes",
                      "YEAR": "1986",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.3 [22.2-22.5]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "YEAR": "2003",
                      "SEX": "Both sexes",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "23.5 [23.4-23.5]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Both sexes",
                      "REGION": "(WHO) Global",
                      "YEAR": "2004",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "23.6 [23.5-23.6]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1976",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "21.9 [21.6-22.3]"
           },
           {
             "dim": {
                      "YEAR": "1979",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.1 [21.8-22.4]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Female",
                      "YEAR": "1980",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "22.2 [21.9-22.4]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "1993",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "23.0 [22.9-23.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "1994",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global"
                    },
             "Value": "23.0 [22.9-23.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "YEAR": "2011",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "24.3 [24.2-24.4]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "SEX": "Female",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "2012",
                      "REGION": "(WHO) Global"
                    },
             "Value": "24.3 [24.2-24.4]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "YEAR": "1987",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "22.2 [22.1-22.4]"
           },
           {
             "dim": {
                      "YEAR": "1988",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Male",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.3 [22.1-22.4]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "SEX": "Male",
                      "YEAR": "2001",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "23.1 [23.1-23.2]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "YEAR": "2002",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "23.2 [23.1-23.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Both sexes",
                      "AGEGROUP": "18+  years",
                      "YEAR": "1987"
                    },
             "Value": "22.4 [22.3-22.5]"
           },
           {
             "dim": {
                      "SEX": "Both sexes",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1988",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.5 [22.4-22.6]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Both sexes",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "YEAR": "2002",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "23.4 [23.4-23.5]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "2005",
                      "SEX": "Both sexes",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global"
                    },
             "Value": "23.6 [23.6-23.7]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "YEAR": "1977",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female"
                    },
             "Value": "22.0 [21.7-22.3]"
           },
           {
             "dim": {
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "SEX": "Female",
                      "YEAR": "1978",
                      "AGEGROUP": "18+  years",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.1 [21.8-22.4]"
           },
           {
             "dim": {
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "YEAR": "1995",
                      "AGEGROUP": "18+  years",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.1 [23.0-23.2]"
           },
           {
             "dim": {
                      "YEAR": "1996",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.2 [23.1-23.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2010",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "24.2 [24.1-24.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "REGION": "(WHO) Global",
                      "YEAR": "2013",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "24.4 [24.3-24.5]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "YEAR": "1985",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Male"
                    },
             "Value": "22.1 [21.9-22.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "YEAR": "1986",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "22.1 [22.0-22.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "2003",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Male",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global"
                    },
             "Value": "23.3 [23.2-23.4]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "2004",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Male",
                      "REGION": "(WHO) Global"
                    },
             "Value": "23.4 [23.3-23.4]"
           },
           {
             "dim": {
                      "AGEGROUP": "18+  years",
                      "YEAR": "1984",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Both sexes",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.2 [22.1-22.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "YEAR": "1991",
                      "SEX": "Both sexes"
                    },
             "Value": "22.7 [22.6-22.7]"
           },
           {
             "dim": {
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1998",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Both sexes",
                      "AGEGROUP": "18+  years",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.1 [23.1-23.2]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2009",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Both sexes"
                    },
             "Value": "23.9 [23.9-24.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "SEX": "Both sexes",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2016"
                    },
             "Value": "24.5 [24.3-24.6]"
           },
           {
             "dim": {
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1981",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Female",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.2 [22.0-22.5]"
           },
           {
             "dim": {
                      "YEAR": "1992",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.9 [22.8-23.0]"
           },
           {
             "dim": {
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1999",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.4 [23.3-23.5]"
           },
           {
             "dim": {
                      "REGION": "(WHO) Global",
                      "SEX": "Female",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "YEAR": "2006",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.9 [23.8-24.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1975"
                    },
             "Value": "21.4 [21.1-21.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "YEAR": "1982",
                      "SEX": "Male",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "21.9 [21.7-22.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Male",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1989",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "22.3 [22.2-22.5]"
           },
           {
             "dim": {
                      "REGION": "(WHO) Global",
                      "SEX": "Male",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "YEAR": "2000",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.1 [23.0-23.2]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "2007",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global"
                    },
             "Value": "23.6 [23.5-23.7]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "SEX": "Male",
                      "YEAR": "2014",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "24.1 [24.0-24.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "YEAR": "1976",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Both sexes"
                    },
             "Value": "21.7 [21.5-22.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "YEAR": "1999",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Both sexes"
                    },
             "Value": "23.2 [23.1-23.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "2015",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global"
                    },
             "Value": "24.2 [24.1-24.4]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "1992",
                      "AGEGROUP": "18+  years",
                      "SEX": "Male",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "22.5 [22.4-22.6]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Male",
                      "YEAR": "1990"
                    },
             "Value": "22.4 [22.3-22.5]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "SEX": "Male",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1983"
                    },
             "Value": "21.9 [21.8-22.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1981",
                      "AGEGROUP": "18+  years",
                      "SEX": "Male",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "21.8 [21.6-22.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "2000",
                      "REGION": "(WHO) Global",
                      "SEX": "Female",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "23.5 [23.4-23.6]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "YEAR": "1998"
                    },
             "Value": "23.3 [23.2-23.4]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1991",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "22.9 [22.7-23.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "YEAR": "1989",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "22.7 [22.6-22.9]"
           },
           {
             "dim": {
                      "YEAR": "2008",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Both sexes",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.9 [23.8-23.9]"
           },
           {
             "dim": {
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Both sexes",
                      "YEAR": "2006",
                      "REGION": "(WHO) Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.7 [23.6-23.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Both sexes",
                      "REGION": "(WHO) Global",
                      "YEAR": "2001"
                    },
             "Value": "23.3 [23.3-23.4]"
           },
           {
             "dim": {
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Both sexes",
                      "YEAR": "1977",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "21.8 [21.6-22.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1979",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Both sexes",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "21.9 [21.7-22.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "1994",
                      "SEX": "Both sexes",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "22.9 [22.8-22.9]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "SEX": "Both sexes",
                      "AGEGROUP": "18+  years",
                      "YEAR": "1996",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "23.0 [22.9-23.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "2011",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "SEX": "Both sexes",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "24.1 [24.0-24.1]"
           },
           {
             "dim": {
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Both sexes",
                      "YEAR": "2013",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "24.2 [24.1-24.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "1986",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "22.5 [22.4-22.7]"
           },
           {
             "dim": {
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1988",
                      "SEX": "Female",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.7 [22.5-22.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Female",
                      "REGION": "(WHO) Global",
                      "YEAR": "2001",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "23.5 [23.4-23.6]"
           },
           {
             "dim": {
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "YEAR": "2003",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.7 [23.6-23.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Female",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "2005",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "23.8 [23.7-23.9]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "SEX": "Male",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "YEAR": "1976"
                    },
             "Value": "21.5 [21.1-21.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1978",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Male",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "21.6 [21.3-21.9]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Male",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "YEAR": "1993"
                    },
             "Value": "22.6 [22.5-22.7]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1995",
                      "REGION": "(WHO) Global"
                    },
             "Value": "22.7 [22.6-22.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2010"
                    },
             "Value": "23.8 [23.7-23.9]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "2012",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "24.0 [23.9-24.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "YEAR": "1975",
                      "REGION": "(WHO) Global",
                      "SEX": "Both sexes",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "21.7 [21.4-21.9]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "YEAR": "1982",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Both sexes",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "22.1 [21.9-22.2]"
           },
           {
             "dim": {
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1989",
                      "SEX": "Both sexes",
                      "REGION": "(WHO) Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.5 [22.4-22.6]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "2000",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Both sexes",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "23.3 [23.2-23.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "2007",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "SEX": "Both sexes"
                    },
             "Value": "23.8 [23.7-23.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "2014",
                      "SEX": "Both sexes",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "24.3 [24.2-24.4]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "SEX": "Female",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "REGION": "(WHO) Global",
                      "YEAR": "1983"
                    },
             "Value": "22.4 [22.2-22.6]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "SEX": "Female",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1990",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "22.8 [22.7-22.9]"
           },
           {
             "dim": {
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1997",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.2 [23.2-23.3]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Female",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "YEAR": "2008",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "24.0 [23.9-24.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Female",
                      "YEAR": "2015",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "24.6 [24.4-24.7]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "YEAR": "1980"
                    },
             "Value": "21.7 [21.5-22.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "YEAR": "1984",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Male"
                    },
             "Value": "22.0 [21.8-22.2]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "YEAR": "1991",
                      "AGEGROUP": "18+  years",
                      "SEX": "Male",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global"
                    },
             "Value": "22.5 [22.4-22.6]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "1998",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "SEX": "Male",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "22.9 [22.8-23.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "REGION": "(WHO) Global",
                      "SEX": "Male",
                      "YEAR": "2005"
                    },
             "Value": "23.4 [23.4-23.5]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2009",
                      "SEX": "Male",
                      "REGION": "(WHO) Global"
                    },
             "Value": "23.7 [23.7-23.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "2016",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Male"
                    },
             "Value": "24.3 [24.1-24.5]"
           },
           {
             "dim": {
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Both sexes",
                      "YEAR": "1978",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "21.8 [21.6-22.1]"
           },
           {
             "dim": {
                      "SEX": "Both sexes",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1980",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.0 [21.8-22.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Both sexes",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1993"
                    },
             "Value": "22.8 [22.7-22.9]"
           },
           {
             "dim": {
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1995",
                      "AGEGROUP": "18+  years",
                      "SEX": "Both sexes",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.9 [22.8-23.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "SEX": "Both sexes",
                      "YEAR": "1997"
                    },
             "Value": "23.1 [23.0-23.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "2010",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Both sexes",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "24.0 [23.9-24.1]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "YEAR": "2012",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Both sexes",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "24.2 [24.1-24.2]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Female",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1985"
                    },
             "Value": "22.5 [22.3-22.7]"
           },
           {
             "dim": {
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1987",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.6 [22.5-22.8]"
           },
           {
             "dim": {
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2002",
                      "AGEGROUP": "18+  years",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.6 [23.5-23.7]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2004",
                      "AGEGROUP": "18+  years",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Female"
                    },
             "Value": "23.7 [23.7-23.8]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1977",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "SEX": "Male",
                      "AGEGROUP": "18+  years"
                    },
             "Value": "21.6 [21.2-21.9]"
           },
           {
             "dim": {
                      "YEAR": "1979",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "REGION": "(WHO) Global",
                      "AGEGROUP": "18+  years",
                      "SEX": "Male",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "21.7 [21.4-21.9]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Male",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "1994",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "REGION": "(WHO) Global"
                    },
             "Value": "22.7 [22.6-22.8]"
           },
           {
             "dim": {
                      "SEX": "Male",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "YEAR": "1996",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "22.8 [22.7-22.9]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "AGEGROUP": "18+  years",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "YEAR": "2011",
                      "REGION": "(WHO) Global",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)",
                      "SEX": "Male"
                    },
             "Value": "23.9 [23.8-24.0]"
           },
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "REGION": "(WHO) Global",
                      "SEX": "Male",
                      "WORLDBANKINCOMEGROUP": "Global",
                      "AGEGROUP": "18+  years",
                      "YEAR": "2013",
                      "GHO": "Mean BMI (kg/m&#xb2;) (crude estimate)"
                    },
             "Value": "24.1 [23.9-24.2]"
           }
          ]
}